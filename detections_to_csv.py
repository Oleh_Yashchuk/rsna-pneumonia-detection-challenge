import numpy as np
import pandas as pd
import tensorflow as tf
import os
from PIL import Image

flags = tf.app.flags
flags.DEFINE_string('model_name', None, 'Path to tf model.')
flags.DEFINE_string('images_path', None, 'Path to images.')
flags.DEFINE_string('output_path', None, 'Path to output detections.')
FLAGS = flags.FLAGS

PATH_TO_FROZEN_GRAPH = FLAGS.model_name + '/frozen_inference_graph.pb'
PATH_TO_TEST_IMAGES_DIR = FLAGS.images_path
TEST_IMAGE_PATHS = [os.path.join(PATH_TO_TEST_IMAGES_DIR, file) for file in os.listdir(PATH_TO_TEST_IMAGES_DIR) if file.endswith('.jpg')]
# print(TEST_IMAGE_PATHS)


detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

def load_image_into_numpy_array(image):
    # The function supports only grayscale images
    last_axis = -1
    dim_to_repeat = 2
    repeats = 3
    grscale_img_3dims = np.expand_dims(image, last_axis)
    training_image = np.repeat(grscale_img_3dims, repeats, dim_to_repeat).astype('uint8')
    assert len(training_image.shape) == 3
    assert training_image.shape[-1] == 3
    return training_image

def run_inference_for_single_image(image, graph):
  with graph.as_default():
    with tf.Session() as sess:
      # Get handles to input and output tensors
      ops = tf.get_default_graph().get_operations()
      all_tensor_names = {output.name for op in ops for output in op.outputs}
      tensor_dict = {}
      for key in [
          'num_detections', 'detection_boxes', 'detection_scores', 'detection_classes'
      ]:
        tensor_name = key + ':0'
        if tensor_name in all_tensor_names:
          tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(tensor_name)
      image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

      # Run inference
      output_dict = sess.run(tensor_dict,
                             feed_dict={image_tensor: np.expand_dims(image, 0)})

      # all outputs are float32 numpy arrays, so convert types as appropriate
      output_dict['num_detections'] = int(output_dict['num_detections'][0])
      output_dict['detection_classes'] = output_dict['detection_classes'][0].astype(np.uint8)
      output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
      output_dict['detection_scores'] = output_dict['detection_scores'][0]
  return output_dict

def get_detections(image_path):
  image = Image.open(image_path)
  image_np = load_image_into_numpy_array(image)
  image_np_expanded = np.expand_dims(image_np, axis=0)
  output_dict = run_inference_for_single_image(image_np, detection_graph)
  return output_dict


# output_dict = get_detections(image_path)
# print(output_dict)

prediction = []
k = 0
for image_path in TEST_IMAGE_PATHS:
  # print('\n\n\n')
  # print(image_path)
  # print('\n\n\n')
  output_dict = get_detections(image_path)
  # print(output_dict)
  patient = image_path.replace(PATH_TO_TEST_IMAGES_DIR,'')
  patient = patient.replace('.jpg','')
  prediction.append({'patientId': patient,
                     'detection_boxes': output_dict['detection_boxes'],
                     'detection_classes': output_dict['detection_classes'],
                     'detection_scores': output_dict['detection_scores'],
                     'num_detections': output_dict['num_detections']})
  k += 1
  if k % 50 == 0:
    print('{} images processed'.format(k))

pdf = pd.DataFrame(prediction)
pdf.to_pickle(FLAGS.output_path)
