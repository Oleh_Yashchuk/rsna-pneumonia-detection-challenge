rsna-pneumonia-detection-challenge

# Трансформация dicom в jpg
convert_dcm_to_jpg.py


# Трансформация jpg в tfrecords
python3 convert_jpg_to_tfrecords.py \
    --input_images_path=data/stage_1_train_images_jpg/ \
    --input_labeling_path=data/stage_1_train_labels.csv \
    --output_path=data/stage_1_train.tfrecord


# Разделение множества tfrecords на насколько частей
python3 split_train_eval.py \
    --input_tf_record=data/stage_1_train.tfrecord-00000-of-00001 \
    --input_labeling_path=data/stage_1_train_labels.csv \
    --input_detailed_info=data/stage_1_detailed_class_info.csv \
    --output_prefix=data/stage_1_train \
    --n_splits=2


# Обучение модели
PIPELINE_CONFIG_PATH=models/faster_rcnn_resnet50/faster_rcnn_resnet50.config
MODEL_DIR=models/faster_rcnn_resnet50/

python3 model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --alsologtostderr

# Запуск tensorboard
tensorboard --logdir=${MODEL_DIR}

# Заморозка модели
MODEL_CP=models/faster_rcnn_resnet50/model.ckpt-191666 # чекпоинт для заморозки
MODEL_FROZEN=model_graph_faster_rcnn_resnet50 # папка в которую будет экспортирован графф

python3 export_inference_graph.py \
    --input_type=image_tensor \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --trained_checkpoint_prefix=${MODEL_CP} \
    --output_directory=${MODEL_FROZEN}


# Загруженность мощностей GPU
watch nvidia-smi


# Формирование файла со списком всех детекций
MODEL_FROZEN=model_graph_faster_rcnn_resnet50
TEST_SET=data/stage_1_test_images_jpg/
TEST_SET_LABELS=data/output/stage_1_test_labels.csv

python3 detections_to_csv.py \
    --model_name=${MODEL_FROZEN} \
    --images_path=${TEST_SET} \
    --output_path=${TEST_SET_LABELS}


# Подбор порога вероятности
treshhold_selection.ipynb