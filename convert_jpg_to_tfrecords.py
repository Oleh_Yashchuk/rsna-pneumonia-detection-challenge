import tensorflow as tf
import pandas as pd
import numpy as np
import contextlib2
import cv2

from utils.dataset_util import int64_feature
from utils.dataset_util import bytes_feature
from utils.dataset_util import float_list_feature
from utils.dataset_util import bytes_list_feature
from utils.dataset_util import int64_list_feature
from utils.visualization_utils import encode_image_array_as_png_str
from dataset_tools.tf_record_creation_util import open_sharded_output_tfrecords

debug = False
num_shards = 1
amount_to_print_progress = 1000

flags = tf.app.flags
flags.DEFINE_string('input_labeling_path', '', 'Path to the CSV input labels')
flags.DEFINE_string('input_images_path', '', 'Path to input images')
flags.DEFINE_string('output_path', '', 'Path to output TFRecord')
FLAGS = flags.FLAGS

def create_tf_example(patientId, boxes):

    image_path = FLAGS.input_images_path + patientId + ".jpg"
    image = cv2.imread(image_path)

    height = image.shape[0] # Image height
    width = image.shape[1] # Image width

    filename = bytes(patientId + '.jpg', 'utf-8') # Filename of the image. Empty if image is not from file
    image_format = b'jpeg' # b'jpeg' or b'png'

    encoded_image_data = encode_image_array_as_png_str(image)

    xmins = [] # List of normalized left x coordinates in bounding box (1 per box)
    xmaxs = [] # List of normalized right x coordinates in bounding box (1 per box)
    ymins = [] # List of normalized top y coordinates in bounding box (1 per box)
    ymaxs = [] # List of normalized bottom y coordinates in bounding box (1 per box)
    classes_text = [] # List of string class name of bounding box (1 per box)
    classes = [] # List of integer class id of bounding box (1 per box)

    for box in boxes:
        if not np.isnan(box[0]):
            if (debug):
                print(box)
            classes_text.append(b'pneumonia')
            classes.append(1)

            # x-min y-min width height
            xmins.append(box[0] / width)   # store normalized values for bbox
            xmaxs.append((box[0] + box[2]) / width)
            ymins.append(box[1] / height)
            ymaxs.append((box[1] + box[3]) / height)

    if (debug):
        print(xmins)
        print(xmaxs)
        print(ymins)
        print(ymaxs)

    tf_example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': int64_feature(height),
        'image/width': int64_feature(width),
        'image/filename': bytes_feature(filename),
        'image/source_id': bytes_feature(filename),
        'image/encoded': bytes_feature(encoded_image_data),
        'image/format': bytes_feature(image_format),
        'image/object/bbox/xmin': float_list_feature(xmins),
        'image/object/bbox/xmax': float_list_feature(xmaxs),
        'image/object/bbox/ymin': float_list_feature(ymins),
        'image/object/bbox/ymax': float_list_feature(ymaxs),
        'image/object/class/text': bytes_list_feature(classes_text),
        'image/object/class/label': int64_list_feature(classes),
    }))
    return tf_example


with contextlib2.ExitStack() as tf_record_close_stack:

    output_tfrecords = open_sharded_output_tfrecords(tf_record_close_stack,
                                                     FLAGS.output_path,
                                                     num_shards)
    train = pd.read_csv(FLAGS.input_labeling_path)
    groups = train.groupby('patientId')

    count = 0

    for patientId in train.drop_duplicates('patientId')['patientId']:

        boxes = groups.get_group(patientId).drop(columns=['patientId']).as_matrix()
        tf_example = create_tf_example(patientId, boxes)

        output_shard_index = count % num_shards
        output_tfrecords[output_shard_index].write(tf_example.SerializeToString())

        count += 1

        if count % amount_to_print_progress == 0:
            print('{} images processed'.format(count))
