# List of files
kaggle competitions files rsna-pneumonia-detection-challenge

# Files downloading
# kaggle competitions download -c rsna-pneumonia-detection-challenge

# Lideraboard
kaggle competitions leaderboard -s rsna-pneumonia-detection-challenge

# My submissions
kaggle competitions submissions rsna-pneumonia-detection-challenge

# Submission
kaggle competitions submit rsna-pneumonia-detection-challenge -f output.csv -m "comment"
