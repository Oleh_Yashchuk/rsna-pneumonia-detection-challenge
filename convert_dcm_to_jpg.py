import pydicom as dicom
import os
import cv2

# make it True if you want in PNG format
PNG = False
amount_to_print_progress = 1000
# Specify the .dcm folder path
folder_path = "data_rsna/test"
# Specify the output jpg/png folder path
jpg_folder_path = "data_rsna/test/"

# folder_path = os.getcwd() + "/" + sys.argv[1]
# jpg_folder_path = os.getcwd() + "/" + sys.argv[1]

images_path = os.listdir(folder_path)
for n, image in enumerate(images_path):
    ds = dicom.dcmread(os.path.join(folder_path, image))
    pixel_array_numpy = ds.pixel_array
    if PNG == False:
        image = image.replace('.dcm', '.jpg')
    else:
        image = image.replace('.dcm', '.png')
    cv2.imwrite(os.path.join(jpg_folder_path, image), pixel_array_numpy)
    if n % amount_to_print_progress == 0:
        print('{} image converted'.format(n))
